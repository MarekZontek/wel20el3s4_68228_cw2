/*
 * EthRecv.c
 *
 *  Created on: Nov 25, 2021
 *      Author: 68228
 */
#include "cw2.h"

#if ZADANIE == 1


#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <linux/if_ether.h> 
#include <unistd.h> 
#include <arpa/inet.h> 
#include <ctype.h> 

int main(void) {
	int i;
	
	unsigned char destination[6]; 
	unsigned char source[6]; 
	unsigned short typ_ramki; 

	unsigned char wersja; 
	unsigned char klasa_ruchu[2]; 
	unsigned int flow_label[3]; 
	unsigned short dlugosc_pakietu; 
	unsigned char nastepnym_naglowek; 
	unsigned char limit_skokow; 
	unsigned char source_address_ip[16];
	unsigned char destination_address_ip[16]; 

	printf("Uruchamiam odbieranie ramek Ethernet.\n"); /* prints */
	unsigned char *buffer = (void*) malloc(ETH_FRAME_LEN);
	int iEthSockHandl = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (iEthSockHandl < 0)
		printf("Problem z otwarciem gniazda : %s!\n", strerror(errno));
	int iDataLen = 0;
	while (1) {
		iDataLen = recvfrom(iEthSockHandl, buffer, ETH_FRAME_LEN, 0, NULL,
		NULL);
		if (iDataLen == -1)
			printf("Nie moge odebrac ramki: %s! \n", strerror(errno));
		else {
			printf("\nOdebrano ramke Ethernet o rozmiarze: %d [B]\n", iDataLen);
		}
		printf("Caly pakiet: ");
		for (i = 0; i < iDataLen; i++) {
			printf("%.2x, ", buffer[i]);
		}
		printf("\n");
		printf("\nWartosci beda wyswietlane w formacie heksadecymalnym\n");


	
		memcpy(&destination, buffer + 0, 6);
		printf(
				"Destination Ethernet address : %.2x %.2x %.2x %.2x %.2x %.2x \n",
				destination[0], destination[1], destination[2], destination[3],
				destination[4], destination[5]);

		memcpy(&source, buffer + 6, 6);
		printf("Source Ethernet address: %.2x %.2x %.2x %.2x %.2x %.2x \n",
				source[0], source[1], source[2], source[3], source[4],
				source[5]);

		memcpy(&typ_ramki, buffer + 12, 2);
		typ_ramki = ntohs(typ_ramki);
		
		printf("Typ ramki: %x \n", typ_ramki);

		
		if (typ_ramki == 0x86dd) {
			printf("To jest pakiet IPv6\n");

			memcpy(&wersja, buffer + 14, 1);
			wersja = wersja & 0xF0;
			printf("Wersja : %.1x \n", wersja);

			
			klasa_ruchu[0] = buffer[14] & 0xF;
			klasa_ruchu[1] = buffer[15] & 0xF0;
			printf("Klasa ruchu : %.1x%.1x \n", klasa_ruchu[0], klasa_ruchu[1]);

			

			flow_label[0] = buffer[15];
			flow_label[1] = buffer[16];
			flow_label[2] = buffer[17];
			printf("Flow label : %.x%.x%.x \n", flow_label[0], flow_label[1],flow_label[2]);

			memcpy(&dlugosc_pakietu, buffer + 18, 2);
			dlugosc_pakietu = ntohs(dlugosc_pakietu);
			printf("Dlugosc payload : %.2x \n", dlugosc_pakietu);

			memcpy(&nastepnym_naglowek, buffer + 20, 2);
			printf("Nastepnym naglowek : %.2x \n", nastepnym_naglowek);

			memcpy(&limit_skokow, buffer + 21, 1);
			printf("Limit skokow : %.2x \n", limit_skokow);

			memcpy(&source_address_ip, buffer + 22, 16);
			printf(
					"Source address : %.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n",
					source_address_ip[0], source_address_ip[1],
					source_address_ip[2], source_address_ip[3],
					source_address_ip[4], source_address_ip[5],
					source_address_ip[6], source_address_ip[7],
					source_address_ip[8], source_address_ip[9],
					source_address_ip[10], source_address_ip[11],
					source_address_ip[12], source_address_ip[13],
					source_address_ip[14], source_address_ip[15]);

			memcpy(&destination_address_ip, buffer + 38, 16);
			printf(
					"Destination address : %.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n",
					destination_address_ip[0], destination_address_ip[1],
					destination_address_ip[2], destination_address_ip[3],
					destination_address_ip[4], destination_address_ip[5],
					destination_address_ip[6], destination_address_ip[7],
					destination_address_ip[8], destination_address_ip[9],
					destination_address_ip[10], destination_address_ip[11],
					destination_address_ip[12], destination_address_ip[13],
					destination_address_ip[14], destination_address_ip[15]);

		}
	}

	return EXIT_SUCCESS;
}
#endif


